#!/usr/bin/fish

set EDITOR "nvim"

alias cat    "bat -pp"
alias config "git --git-dir $HOME/.cfg/ --work-tree=$HOME"
alias gcm    "git commit -S -m"
alias gco    "git checkout"
alias gl     "git pull"
alias gp     "git push"
alias gpnew  'git push -u origin (rev-parse --abbrev-ref HEAD)'
alias gs     "git status"

eval (starship init fish)
