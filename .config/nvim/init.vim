call plug#begin('~/.config/nvim/plugged')
Plug 'itchyny/lightline.vim'
Plug 'airblade/vim-rooter'
Plug 'tpope/vim-commentary'
Plug 'vimwiki/vimwiki'
Plug 'junegunn/goyo.vim'

" IDE
Plug 'preservim/nerdtree'
Plug 'ryanoasis/vim-devicons'
Plug 'mileszs/ack.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'junegunn/fzf', {'do': 'fzf#install()'}
Plug 'junegunn/fzf.vim'
Plug 'dense-analysis/ale'
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Language specific
Plug 'rust-lang/rust.vim'
Plug 'mboughaba/i3config.vim'
Plug 'hashivim/vim-terraform'
Plug 'cespare/vim-toml'
Plug 'peitalin/vim-jsx-typescript'
Plug 'jvirtanen/vim-hcl'

" Color Scheme
Plug 'arcticicestudio/nord-vim'
Plug 'dracula/vim', { 'as': 'dracula' }
call plug#end()

" Config
filetype plugin indent on
set cmdheight=2
set completeopt=noinsert,menuone,noselect
set cursorline
set expandtab
set hidden
set ignorecase
set incsearch
set mouse=""
set number
set relativenumber
set scrolloff=50
set shiftwidth=4
set shortmess+=c
set signcolumn=yes
set smartcase
set softtabstop=0
set spelllang=en_gb
set splitbelow
set splitright
set termguicolors
set tabstop=4
set updatetime=300
set wildignore=.git/*,.venv/*

syntax on
colorscheme dracula

let mapleader = ' '

let g:ack_autoclose = 1
let g:ack_use_cword_for_empty_search = 1

let g:ale_set_highlights = 0
let g:ale_rust_cargo_use_check = 1

let g:coc_global_extensions = []
let g:coc_global_extensions += ['coc-json']
let g:coc_global_extensions += ['coc-rust-analyzer']
let g:coc_global_extensions += ['coc-yaml']
let g:coc_global_extensions += ['coc-eslint']
let g:coc_global_extensions += ['coc-prettier']
let g:coc_global_extensions += ['coc-sql']

let g:fzf_action = {
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }
let g:fzf_layout = {'window': { 'width': 1, 'height': 1, 'highlight': 'Normal', 'border': 'sharp' } }
let $FZF_DEFAULT_OPTS = '--layout=reverse --inline-info'
let $FZF_DEFAULT_COMMAND="rg --files --hidden --glob '!.git/**'"

let g:lightline = {
      \ 'colorscheme': 'dracula',
      \ }

let NERDTreeQuitOnOpen = 1
let NERDTreeWinSize = 55

let g:terraform_fmt_on_save = 1

if executable("rg")
    let g:ackprg = 'rg --vimgrep --type-not sql --smart-case'
    set grepprg=rg\ --vimgrep\ --no-heading
    set grepformat=%f:%l:%c:%m,%f:%l:%m
    let g:fzf_tags_command = 'rg --files | ctags --links=no -R -L-'
endif

" Functions
function! RemoveTrailingWhiteSpace()
    :normal mw
    :%s/\s\+$//e
    :normal `w
endfunction

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

source $HOME/.config/nvim/rnr.vim

" Autocommands
autocmd FileType make setlocal noexpandtab
autocmd FileType yaml,hcl setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2
autocmd FileType go setlocal noexpandtab tabstop=4 shiftwidth=4 softtabstop=4
autocmd BufNewFile,BufRead *.j2 set ft=jinja
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif
autocmd BufWritePre * call RemoveTrailingWhiteSpace()

" Abbreviations
cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qa! qa!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qa qa
cnoreabbrev Qall qall
cnoreabbrev Ack Ack!

" Key bindings
if exists('*complete_info')
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <C-P> <Esc>:Files<CR>
inoremap <A-t> <Esc>:call RnrTermToggle()<CR>

nnoremap <esc> :noh<CR><esc>
nnoremap n nzzzv
nnoremap <leader>tt :call RnrToggle()<CR>
nnoremap <leader>. :Files<CR>
nnoremap <C-p> :Files<CR>
nnoremap <leader>sp :Ack
nnoremap <leader>st :Tags<CR>
nnoremap <leader>, :Buffers<CR>
nnoremap <leader>op :NERDTreeToggle<CR>
nnoremap <leader>bc :%bd\|e#<CR>
nnoremap <leader>gy :Goyo 50%<cr>
nnoremap <leader>ss :set spell!<CR>
nnoremap <silent> [q :cprevious<CR>
nnoremap <silent> ]q :cnext<CR>
nmap <silent> <leader>cd <Plug>(coc-definition)
nnoremap <silent> K :call <SID>show_documentation()<CR>
nmap <leader>cr <Plug>(coc-rename)
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)
nmap <leader><left> <C-w>h
nmap <leader><down> <C-w>j
nmap <leader><up> <C-w>k
nmap <leader><right> <C-w>l

tnoremap <leader>tt <C-\><C-n>:call RnrToggle()<cr>

vnoremap > >gv
vnoremap < <gv
