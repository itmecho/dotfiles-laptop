nnoremap <silent> <leader>ct :call<Space>RnrExec("cargo test")<CR>
nnoremap <silent> <leader>cc :call<Space>RnrExec("cargo build")<CR>
nnoremap <silent> <leader>cr :call<Space>RnrExec("cargo run")<CR>
